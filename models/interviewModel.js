import { Schema, model } from 'mongoose'

const interviewSchema = Schema(
    {
        company: {
            type: String,
            required: [true, 'The company name has to be specified'],
        },
        creator: {
            type: String,
            required: [true, 'The user id of a creator has to be specified'],
        },
        status: {
            type: String,
            enum: [
                'In progress',
                'Finished',
                'Company answer awaiting',
                'My answer awaiting',
            ],
            default: 'In progress',
            required: false,
        },
        events: [{ type: Schema.Types.ObjectId, ref: 'Event' }],
    },
    {
        timestamps: true,
    }
)

const Interview = model('Interview', interviewSchema)
export default Interview
