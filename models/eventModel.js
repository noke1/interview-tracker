import { Schema, model } from 'mongoose'

const eventSchema = Schema(
    {
        creator: {
            type: String,
            required: true,
        },
        interview: {
            type: Schema.Types.ObjectId,
            ref: 'Interview',
        },
        initiator: {
            type: String,
        },
        eventType: {
            type: String,
            lowercase: true,
            enum: ['email', 'phone call'],
        },
        comment: {
            type: String,
        },
    },
    {
        timestamps: true,
    }
)

const Event = model('Event', eventSchema)
export default Event
