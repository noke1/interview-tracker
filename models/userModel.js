import { Schema, model } from 'mongoose'

const userSchema = Schema(
    {
        username: {
            type: String,
            unique: true,
            required: true,
        },
        password: {
            type: String,
            required: true,
        },
    },
    {
        timestamp: true,
    }
)

const User = model('User', userSchema)
export default User
