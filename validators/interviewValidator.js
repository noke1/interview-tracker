import { body } from 'express-validator'
import * as InterviewService from '../services/interviewService.js'

const INTERVIEW_MIN_LENGTH = 3
const INTERVIEW_MAX_LENGTH = 20

const checkInterviewNameUsed = async (interviewName) => {
    const interview = await InterviewService.exists({ company: interviewName })
    if (interview)
        throw new Error(
            'There is already an interview created for that company'
        )
}
const interviewValidation = [
    body('company')
        .trim()
        .notEmpty()
        .withMessage('Interview name/company cannot be empty'),
    body('company').trim().custom(checkInterviewNameUsed),
    body('company')
        .trim()
        .isLength({ min: INTERVIEW_MIN_LENGTH, max: INTERVIEW_MAX_LENGTH })
        .withMessage(
            `Company has to be between ${INTERVIEW_MIN_LENGTH} and ${INTERVIEW_MAX_LENGTH} characters long`
        ),
]

export { interviewValidation }
