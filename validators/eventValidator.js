import { body } from 'express-validator'

const COMMENT_MAX_LENGTH = 250
const INITIATOR_MIN_LENGTH = 3
const INITIATOR_MAX_LENGTH = 40

const eventValidation = [
    body('initiator')
        .trim()
        .notEmpty()
        .withMessage('Initiator cannot be empty'),
    body('initiator')
        .trim()
        .isLength({ min: INITIATOR_MIN_LENGTH, max: INITIATOR_MAX_LENGTH })
        .withMessage(
            `Initiator has to be between ${INITIATOR_MIN_LENGTH} and ${INITIATOR_MAX_LENGTH} characters long`
        ),

    body('comment')
        .trim()
        .isLength({ max: COMMENT_MAX_LENGTH })
        .withMessage(
            `Comment has to be up to ${COMMENT_MAX_LENGTH} characters long`
        ),
]

export { eventValidation }
