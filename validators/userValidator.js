import { body } from 'express-validator'
import * as UserService from '../services/userService.js'

const USERNAME_MIN_LENGTH = 3
const USERNAME_MAX_LENGTH = 20
const PASSWORD_MIN_LENGTH = 3
const PASSWORD_MAX_LENGTH = 20

const checkUsernameUsed = async (username) => {
    const user = await UserService.exists({ username })
    if (user) throw new Error('Username already in use')
}
const isAlphaNumeric = (username) => username.match(/^[A-Za-z0-9]+$/)

const signupValidation = [
    body('username').trim().notEmpty().withMessage('Username cannot be empty'),
    body('username').trim().custom(checkUsernameUsed),
    body('username')
        .trim()
        .custom(isAlphaNumeric)
        .withMessage('Username can contain only letters and numbers'),
    body('username')
        .trim()
        .isLength(USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)
        .withMessage(
            `Username has to be between ${USERNAME_MIN_LENGTH} and ${USERNAME_MAX_LENGTH} characters long`
        ),

    body('password').trim().notEmpty().withMessage('Password cannot be empty'),
    body('password')
        .trim()
        .isLength(PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH)
        .withMessage(
            `Password has to be between ${PASSWORD_MIN_LENGTH} and ${PASSWORD_MAX_LENGTH} characters long`
        ),
]

export { signupValidation }
