import mongoose from 'mongoose'
import dotenv from 'dotenv'
import dotenvExpand from 'dotenv-expand'

const config = dotenv.config()
dotenvExpand.expand(config)

export const connectToDatabase = async () => {
    try {
        console.log('ADDRESS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', process.env.MONGODB_ADDRESS)
        await mongoose.connect(process.env.MONGODB_ADDRESS)
        console.log('Connected to MongoDB')
    } catch (err) {
        console.error(err)
    }
}
