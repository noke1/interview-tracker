import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
import dotenvExpand from 'dotenv-expand'

const config = dotenv.config()
dotenvExpand.expand(config)
const { APP_SECRET = 'secret' } = process.env

const generate = (payload = {}) => {
    const token = jwt.sign(payload, APP_SECRET)
    return token
}

const verify = (token) => {
    return jwt.verify(token, APP_SECRET)
}

export { generate, verify }
