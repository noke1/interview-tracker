import bcrypt from 'bcryptjs'

const SALT_ROUNDS = 9

const compare = async (first, second) => {
    const isMatching = await bcrypt.compare(first, second)
    return isMatching
}

const hash = async (value) => {
    const hashedValue = await bcrypt.hash(value, SALT_ROUNDS)
    return hashedValue
}

export { compare, hash }
