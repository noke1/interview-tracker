import * as Token from '../utils/auth.js'
import * as UserService from '../services/userService.js'

export const checkLoggedIn = async (req, res, next) => {
    try {
        const user = await extractUserFromToken(req, res)
        req.user = user
        next()
    } catch (error) {
        return res.status(500).json({ message: error })
    }
}

export const addCreatorIdToBody = async (req, res, next) => {
    try {
        const creator = await extractUserFromToken(req, res)
        const creatorId = creator._id.valueOf()
        req.body.creator = creatorId
        next()
        return
    } catch (error) {
        return res.status(500).json({ message: error })
    }
}

export const checkResourceAccess = function (getResourceById, resourceName) {
    return async function (req, res, next) {
        const { id } = req.params
        try {
            const resource = await getResourceById(id)
            if (!resource) {
                return res.status(404).json({
                    message: 'Resource not found!',
                })
            }
            if (req.user._id.valueOf() != resource.creator) {
                return res.status(403).json({
                    message: `You do not have the access to the requested ${resourceName}!`,
                })
            }
            req[resourceName] = resource
            next()
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }
    }
}

const extractUserFromToken = async (req, res) => {
    if (!req.headers.authorization) {
        return res.status(400).json({ message: 'No authorization header' })
    }

    const [, token] = req.headers.authorization.split(' ')
    if (!token) {
        return res.status(400).json({ message: 'Malformed auth header' })
    }

    const payload = Token.verify(token)
    if (!payload) {
        return res.status(400).json({ message: 'Token verification failed' })
    }

    const user = await UserService.findOne({ username: payload.username })
    return user
}
