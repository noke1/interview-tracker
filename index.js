import express from 'express'
import Mongoose from 'mongoose'
import dotenv from 'dotenv'
import dotenvExpand from 'dotenv-expand'
import cors from 'cors'
import interviewRoutes from './routes/interviewRoutes.js'
import eventRoutes from './routes/eventRoutes.js'
import resetRoutes from './routes/resetRoutes.js'
import signupRoutes from './routes/signupRoutes.js'
import loginRoutes from './routes/loginRoutes.js'
import { connectToDatabase } from './utils/database.js'

const config = dotenv.config()
dotenvExpand.expand(config)

const app = express()
app.use(express.json())
app.use(cors())
app.use('/api/interviews', interviewRoutes)
app.use('/api/events', eventRoutes)
app.use('/api/reset', resetRoutes)
app.use('/api/signup', signupRoutes)
app.use('/api/login', loginRoutes)

await connectToDatabase()
try {
    app.listen(process.env.APP_PORT, () => {
        console.log('App started!')
    })
} catch (err) {
    console.error(err)
}

export default app
