import express from 'express'
import * as InterviewService from '../services/interviewService.js'
import { validationResult } from 'express-validator'
import { interviewValidation } from '../validators/interviewValidator.js'
import {
    checkLoggedIn,
    addCreatorIdToBody,
    checkResourceAccess,
} from '../middlewares/middleware.js'

const router = express.Router()

router.get('/', [checkLoggedIn, addCreatorIdToBody], async (req, res) => {
    try {
        const allInterviews = await InterviewService.getByCreator(
            req.body.creator
        )
        return res.status(200).json(allInterviews)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
})

router.get(
    '/:id',
    [checkLoggedIn, checkResourceAccess(InterviewService.getById, 'interview')],
    async (req, res) => {
        return res.status(200).json(req.interview)
    }
)

router.get(
    '/:id/events',
    [checkLoggedIn, checkResourceAccess(InterviewService.getById, 'interview')],
    async (req, res) => {
        return res.status(200).json(req.interview.events)
    }
)

router.put(
    '/:id',
    [
        checkLoggedIn,
        interviewValidation,
        checkResourceAccess(InterviewService.getById, 'interview'),
    ],
    async (req, res) => {
        try {
            const error = validationResult(req)
            if (!error.isEmpty()) {
                const errors = error.array().map((err) => err.msg)
                return res.status(422).json({ message: errors })
            }

            const newInterview = await InterviewService.getByIdAndUpdate(
                req.params.id,
                req.body
            )

            return res.status(200).json(newInterview)
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }
    }
)

router.delete(
    '/:id',
    [checkLoggedIn, checkResourceAccess(InterviewService.getById, 'interview')],
    async (req, res) => {
        const { id } = req.params
        try {
            await InterviewService.getByIdAndDelete(id)

            return res.status(200).json({
                message: `Interview with id: ${id} deleted successfully!!`,
            })
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }
    }
)

router.post(
    '/',
    [checkLoggedIn, interviewValidation, addCreatorIdToBody],
    async (req, res) => {
        try {
            const error = validationResult(req)
            if (!error.isEmpty()) {
                const errors = error.array().map((err) => err.msg)
                return res.status(422).json({ message: errors })
            }

            const interview = await InterviewService.add(req.body)
            return res.status(201).json(interview)
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }
    }
)

export default router
