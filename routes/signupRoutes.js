import express from 'express'
import { validationResult } from 'express-validator'
import { signupValidation } from '../validators/userValidator.js'
import * as UserService from '../services/userService.js'
import * as Hash from '../utils/hash.js'

const router = express.Router()

router.post('/', signupValidation, async (req, res) => {
    const { username, password } = req.body
    try {
        const error = validationResult(req)
        if (!error.isEmpty()) {
            const errors = error.array().map((err) => err.msg)
            return res.status(422).json({ message: errors })
        }

        const hashedPassword = await Hash.hash(password)
        const user = await UserService.add({
            username,
            password: hashedPassword,
        })

        res.status(201).json(user)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

export default router
