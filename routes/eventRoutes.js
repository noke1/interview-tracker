import express from 'express'
import * as EventService from '../services/eventService.js'
import * as InterviewService from '../services/interviewService.js'
import { eventValidation } from '../validators/eventValidator.js'
import { validationResult } from 'express-validator'
import {
    checkLoggedIn,
    addCreatorIdToBody,
    checkResourceAccess,
} from '../middlewares/middleware.js'

const router = express.Router()

router.get(
    '/:id',
    [checkLoggedIn, checkResourceAccess(EventService.getById, 'event')],
    async (req, res) => {
        return res.status(200).json(req.event)
    }
)

router.delete(
    '/:id',
    [checkLoggedIn, checkResourceAccess(EventService.getById, 'event')],
    async (req, res) => {
        const { id } = req.params
        try {
            await EventService.getByIdAndDelete(id)
            return res.status(200).json({
                message: `Event with an id: ${id} deleted successfully!!`,
            })
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }
    }
)

router.put(
    '/:id',
    [
        checkLoggedIn,
        eventValidation,
        checkResourceAccess(EventService.getById, 'event'),
    ],
    async (req, res) => {
        const { id } = req.params
        try {
            const error = validationResult(req)
            if (!error.isEmpty()) {
                const errors = error.array().map((err) => err.msg)
                return res.status(422).json({ message: errors })
            }

            await EventService.getByIdAndUpdate(id, req.body)
            const updatedEvent = await EventService.getById(id)
            return res.status(200).json(updatedEvent)
        } catch (error) {
            return res.status(500).json({ message: error.message })
        }
    }
)

router.post(
    '/',
    [checkLoggedIn, eventValidation, addCreatorIdToBody],
    async (req, res) => {
        const event = req.body
        try {
            const error = validationResult(req)
            if (!error.isEmpty()) {
                const errors = error.array().map((err) => err.msg)
                return res.status(422).json({ message: errors })
            }

            const newEvent = await EventService.add(event)
            const interview = await InterviewService.getById(newEvent.interview)
            interview.events.push(newEvent)

            await InterviewService.edit(interview)
            return res.status(201).json(newEvent)
        } catch (error) {
            console.log(error)
            return res.status(500).json({ message: error.message })
        }
    }
)

export default router
