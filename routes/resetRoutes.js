import express from 'express'
import * as EventService from '../services/eventService.js'
import * as InterviewService from '../services/interviewService.js'
import * as UserService from '../services/userService.js'

const router = express.Router()

router.get('/', async (req, res) => {
    try {
        await InterviewService.deleteByQuery()
        await EventService.deleteByQuery()
        await UserService.deleteByQuery()

        return res.status(200).json({
            message: 'Collections of Interview, Event, User were cleared!',
        })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
})

export default router
