import express from 'express'
import * as Hash from '../utils/hash.js'
import * as Token from '../utils/auth.js'
import * as UserService from '../services/userService.js'

const router = express.Router()

router.post('/', async (req, res) => {
    const { username, password } = req.body
    try {
        const user = await UserService.findOne({ username })
        if (!user) {
            return res.status(400).json({ message: 'User does not exist!' })
        }

        const result = await Hash.compare(password, user.password)
        if (!result) {
            return res.status(400).json({ message: 'Password does not match!' })
        }

        const token = Token.generate({ username })
        res.status(200).json({ message: 'Token created!', token })
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

export default router
