import app from '../index.js'
import request from 'supertest'
import { expect } from 'chai'

const INTERVIEWS_ROUTE = '/api/interviews'
const EVENTS_ROUTE = '/api/events'
const SIGNUP_ROUTE = '/api/signup'
const LOGIN_ROUTE = '/api/login'
const INTERVIEWS_EVENTS_ROUTE = (interviewId) =>
    `${INTERVIEWS_ROUTE}/${interviewId}/events`
let bearerToken

before(async () => {
    const user = {
        username: 'apitestinter',
        password: 'apitestinter',
    }
    await request(app).post(SIGNUP_ROUTE).send(user)
    const login = await request(app).post(LOGIN_ROUTE).send(user)
    bearerToken = login.body.token
})

describe('GET /interviews', () => {
    it('Should return all interviews in the database', async () => {
        const interviews = [
            {
                company: 'Apple_inter_GET1',
            },
            {
                company: 'Apple_inter_GET2',
            },
        ]
        for (const interview of interviews) {
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        }

        const allInterviews = await request(app)
            .get(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)

        expect(allInterviews.statusCode).to.equal(200)
        expect(allInterviews.body).to.have.length.greaterThanOrEqual(
            interviews.length
        )
    })

    it('Should return one interview by ID and have default values', async () => {
        const interview = {
            company: 'Apple_inter_GET3',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body

        const interviewById = await request(app)
            .get(`${INTERVIEWS_ROUTE}/${interviewId}`)
            .set('Authorization', `bearer ${bearerToken}`)

        expect(interviewById.statusCode).to.equal(200)
        expect(interviewById.body).to.be.an('object')
        expect(interviewById.body).to.have.property('_id', `${interviewId}`)
        expect(interviewById.body).to.have.property(
            'status',
            'In progress',
            'The default status value should be In progress'
        )
        expect(interviewById.body.events).to.be.an('array').and.eql([])
    })

    it('Should return interview by ID have event inside', async () => {
        const interview = {
            company: 'Apple_inter_GET4',
        }

        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const event = {
            interview: interviewId,
            initiator: 'company',
            eventType: 'phone call',
            comment: 'The HR girl called me',
        }
        await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)

        const interviewById = await request(app)
            .get(`${INTERVIEWS_ROUTE}/${interviewId}`)
            .set('Authorization', `bearer ${bearerToken}`)

        expect(interviewById.statusCode).to.equal(200)
        expect(interviewById.body.events).to.be.an('array').and.have.lengthOf(1)
        expect(interviewById.body.events[0]).to.include(event)
    })

    it('Should return an array of events assigned to an interview', async () => {
        const interview = {
            company: 'Apple_inter_GET5',
        }

        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const events = [
            {
                interview: interviewId,
                initiator: 'company',
                eventType: 'phone call',
                comment: 'The HR girl called me',
            },
            {
                interview: interviewId,
                initiator: 'company',
                eventType: 'email',
                comment: 'The HR girl wrote me an email',
            },
        ]
        for (const event of events) {
            await request(app)
                .post(EVENTS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(event)
        }

        const interviewById = await request(app)
            .get(`${INTERVIEWS_EVENTS_ROUTE(interviewId)}`)
            .set('Authorization', `bearer ${bearerToken}`)

        expect(interviewById.statusCode).to.equal(200)
        expect(interviewById.body)
            .to.be.an('array')
            .and.have.lengthOf(events.length)
        expect(interviewById.body[0]).to.be.an('object').include(events[0])
        expect(interviewById.body[1]).to.be.an('object').include(events[1])
    })
})

describe('POST /interviews', () => {
    it('Should add new interview and return correct object', async () => {
        const interview = {
            company: 'Apple_inter_POST1',
        }
        const newInterview = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        expect(newInterview.statusCode).to.equal(201)
        expect(newInterview.body).to.have.property('_id').that.is.not.empty
        expect(newInterview.body).to.be.an('object')
        expect(newInterview.body)
            .to.have.property('status')
            .that.eql(
                'In progress',
                'The default status value should be In progress'
            )
        expect(newInterview.body.company).to.equal(interview.company)
        expect(newInterview.body.events).to.be.an('array').that.is.empty
    })

    it('Should return a validation error when the interview name is empty', async () => {
        const interview = {
            company: '',
        }
        const response = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Interview name/company cannot be empty',
            'Company has to be between 3 and 20 characters long',
        ])
    })

    it('Should return a validation error when the interview name is below 3 characters', async () => {
        const interview = {
            company: 'AC',
        }
        const response = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Company has to be between 3 and 20 characters long',
        ])
    })

    it('Should return a validation error when the interview name more than 20 characters', async () => {
        const interview = {
            company: 'AC4567890asdrftgyhujk',
        }
        const response = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Company has to be between 3 and 20 characters long',
        ])
    })
})

describe('DELETE /interviews', () => {
    it('Should remove the interview with specific id', async () => {
        const interview = {
            company: 'Apple_inter_DELETE1',
        }
        const newInterview = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        const response = await request(app)
            .delete(`${INTERVIEWS_ROUTE}/${newInterview.body._id}`)
            .set('Authorization', `bearer ${bearerToken}`)

        expect(response.statusCode).to.equal(200)
        expect(response.body.message).to.equal(
            `Interview with id: ${newInterview.body._id} deleted successfully!!`
        )
    })
})

describe('PUT /interviews', () => {
    it('Should change only the interview company and status when there are no events in the interview', async () => {
        const interview = {
            company: 'Apple_inter_PUT1',
        }
        const newInterview = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        const changedInterview = {
            company: 'Changed company',
            status: 'Finished',
        }
        const response = await request(app)
            .put(`${INTERVIEWS_ROUTE}/${newInterview.body._id}`)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(changedInterview)

        expect(response.statusCode).to.equal(200)
        expect(response.body).to.have.property('_id').that.is.not.empty
        expect(response.body).to.be.an('object')
        expect(response.body)
            .to.have.property('status')
            .that.eql(changedInterview.status)
        expect(response.body.company).to.equal(changedInterview.company)
        expect(response.body.events).to.be.an('array').that.is.empty
    })

    it('Should change only the interview company and status when there is 1 event in the interview', async () => {
        const interview = {
            company: 'Apple_inter_PUT2',
        }
        const newInterview = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        const event = {
            interview: newInterview.body._id,
            initiator: 'company',
            eventType: 'phone call',
            comment: 'The HR girl called me',
        }
        await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)

        const changedInterview = {
            company: 'Apple_inter_PUT3',
            status: 'Finished',
        }
        const response = await request(app)
            .put(`${INTERVIEWS_ROUTE}/${newInterview.body._id}`)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(changedInterview)

        expect(response.statusCode).to.equal(200)
        expect(response.body).to.have.property('_id').that.is.not.empty
        expect(response.body).to.be.an('object')
        expect(response.body)
            .to.have.property('status')
            .that.eql(changedInterview.status)
        expect(response.body.company).to.equal(changedInterview.company)
        expect(response.body.events).to.be.an('array').that.is.not.empty
        expect(response.body.events[0]).to.include(event)
    })

    it('Should return a validation error when the interview name is empty', async () => {
        const interview = {
            company: 'Apple_inter_PUT4',
        }
        const newInterview = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        const changedInterview = {
            company: '',
            status: 'Finished',
        }
        const response = await request(app)
            .put(`${INTERVIEWS_ROUTE}/${newInterview.body._id}`)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(changedInterview)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Interview name/company cannot be empty',
            'Company has to be between 3 and 20 characters long',
        ])
    })

    it('Should return a validation error when the interview name is below 3 characters', async () => {
        const interview = {
            company: 'Apple_inter_PUT5',
        }
        const newInterview = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        const changedInterview = {
            company: 'AC',
            status: 'Finished',
        }
        const response = await request(app)
            .put(`${INTERVIEWS_ROUTE}/${newInterview.body._id}`)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(changedInterview)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Company has to be between 3 and 20 characters long',
        ])
    })

    it('Should return a validation error when the interview name more than 20 characters', async () => {
        const interview = {
            company: 'Apple_inter_PUT6',
        }
        const newInterview = await request(app)
            .post(INTERVIEWS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(interview)

        const changedInterview = {
            company: 'AAAAAAAAAAOOOOOOOOOO3',
            status: 'Finished',
        }
        const response = await request(app)
            .put(`${INTERVIEWS_ROUTE}/${newInterview.body._id}`)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(changedInterview)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Company has to be between 3 and 20 characters long',
        ])
    })
})
