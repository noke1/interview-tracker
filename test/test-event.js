import app from '../index.js'
import request from 'supertest'
import { expect } from 'chai'

const EVENTS_ROUTE = '/api/events'
const INTERVIEWS_ROUTE = '/api/interviews'
const SIGNUP_ROUTE = '/api/signup'
const LOGIN_ROUTE = '/api/login'
let bearerToken

before(async () => {
    const user = {
        username: 'apitestevent',
        password: 'apitestevent',
    }
    await request(app).post(SIGNUP_ROUTE).send(user)
    const login = await request(app).post(LOGIN_ROUTE).send(user)
    bearerToken = login.body.token
})

describe('GET /events', () => {
    it('Should return 1 specific event by id', async () => {
        const interview = {
            company: 'Google_events_GET1',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body

        const event = {
            interview: interviewId,
            initiator: 'company',
            eventType: 'phone call',
            comment: 'The HR girl called me',
        }
        const newEvent = await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)

        const eventById = await request(app)
            .get(`${EVENTS_ROUTE}/${newEvent.body._id}`)
            .set('Authorization', `bearer ${bearerToken}`)

        expect(eventById.statusCode).to.equal(200)
        expect(eventById.body).to.be.an('object')
        expect(eventById.body).to.include({
            initiator: event.initiator,
            eventType: event.eventType,
            comment: event.comment,
        })
        expect(eventById.body)
            .to.have.property('_id')
            .and.equal(`${newEvent.body._id}`)
        expect(eventById.body.interview)
            .to.include(interview)
            .and.have.property('_id').that.is.not.empty
    })
})

describe('POST /events', () => {
    it('Should add a new event and return a proper event object', async () => {
        const interview = {
            company: 'Google_events_POST1',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const event = {
            interview: interviewId,
            initiator: 'company',
            eventType: 'phone call',
            comment: 'The HR girl called me',
        }
        const newEvent = await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)

        expect(newEvent.statusCode).to.equal(201)
        expect(newEvent.body).to.be.an('object')
        expect(newEvent.body).to.include({
            initiator: event.initiator,
            eventType: event.eventType,
            comment: event.comment,
        })
        expect(newEvent.body).to.have.property('_id').that.is.not.empty
        expect(newEvent.body)
            .and.have.property('interview')
            .that.equal(interviewId)
    })

    it('Should return a validation error when the comment is above 250', async () => {
        const interview = {
            company: 'Google_events_POST2',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const event = {
            interview: interviewId,
            initiator: 'company',
            eventType: 'phone call',
            comment:
                'aaaaaaaaaabbbbbbbbbbccccccccccppppppppppllllllllllaaaaaaaaaabbbbbbbbbbccccccccccppppppppppllllllllllaaaaaaaaaabbbbbbbbbbccccccccccppppppppppllllllllllaaaaaaaaaabbbbbbbbbbccccccccccppppppppppllllllllllaaaaaaaaaabbbbbbbbbbccccccccccppppppppppllllllllllX',
        }
        const response = await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Comment has to be up to 250 characters long',
        ])
    })

    it('Should return a validation error when the initiator is below 3', async () => {
        const interview = {
            company: 'Google_events_POST3',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const event = {
            interview: interviewId,
            initiator: 'AC',
            eventType: 'phone call',
            comment: 'test comment',
        }
        const response = await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Initiator has to be between 3 and 40 characters long',
        ])
    })

    it('Should return a validation error when the initiator is greater than 40', async () => {
        const interview = {
            company: 'Google_events_POST4',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const event = {
            interview: interviewId,
            initiator: 'AAAAAAAAAACCCCCCCCCCOOOOOOOOOOPPPPPPPPPPR',
            eventType: 'phone call',
            comment: 'test comment',
        }
        const response = await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Initiator has to be between 3 and 40 characters long',
        ])
    })

    it('Should return a validation error when the initiator is empty', async () => {
        const interview = {
            company: 'Google_events_POST5',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const event = {
            interview: interviewId,
            initiator: '',
            eventType: 'phone call',
            comment: 'test comment',
        }
        const response = await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.deep.equal([
            'Initiator cannot be empty',
            'Initiator has to be between 3 and 40 characters long',
        ])
    })
})

describe('DELETE /events', () => {
    it('Should delete the event with specific id with a proper confirmation message', async () => {
        const interview = {
            company: 'Google_events_DEL1',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const event = {
            interview: interviewId,
            initiator: 'company',
            eventType: 'phone call',
            comment: 'The HR girl called me',
        }
        const newEvent = await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)

        const deleted = await request(app)
            .delete(`${EVENTS_ROUTE}/${newEvent.body._id}`)
            .set('Authorization', `bearer ${bearerToken}`)

        expect(deleted.statusCode).to.equal(200)
        expect(deleted.body)
            .to.have.property('message')
            .that.equal(
                `Event with an id: ${newEvent.body._id} deleted successfully!!`
            )
    })

    it('Should return a proper message when a non-existing Id passed', async () => {
        const fakeId = '000000000000000000000000'
        const deleted = await request(app)
            .delete(`${EVENTS_ROUTE}/${fakeId}`)
            .set('Authorization', `bearer ${bearerToken}`)

        expect(deleted.statusCode).to.equal(404)
        expect(deleted.body)
            .to.have.property('message')
            .that.equal('Resource not found!')
    })
})

describe('PUT /events', () => {
    it('Should change the event details to the new one', async () => {
        const interview = {
            company: 'Google_events_PUT1',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const event = {
            interview: interviewId,
            initiator: 'company',
            eventType: 'phone call',
            comment: 'The HR girl called me',
        }
        const { _id: eventId } = await (
            await request(app)
                .post(EVENTS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(event)
        ).body

        const newEvent = {
            interview: interviewId,
            initiator: 'companyChanged',
            eventType: 'email',
            comment: 'Changed the comment',
        }
        const editedEvent = await request(app)
            .put(`${EVENTS_ROUTE}/${eventId}`)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(newEvent)

        expect(editedEvent.statusCode).to.equal(200)
        expect(editedEvent.body).to.be.an('object')
        expect(editedEvent.body).to.include({
            initiator: newEvent.initiator,
            eventType: newEvent.eventType,
            comment: newEvent.comment,
        })
        expect(editedEvent.body).to.have.deep.own.property('interview', {
            _id: interviewId,
            company: interview.company,
        })
    })

    it('Should return a validation error when the initiator is empty', async () => {
        const interview = {
            company: 'Google_events_PUT2',
        }
        const { _id: interviewId } = await (
            await request(app)
                .post(INTERVIEWS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(interview)
        ).body
        const event = {
            interview: interviewId,
            initiator: 'test event',
            eventType: 'phone call',
            comment: 'test comment',
        }
        const response = await request(app)
            .post(EVENTS_ROUTE)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(event)
        const { _id: eventId } = await (
            await request(app)
                .post(EVENTS_ROUTE)
                .set('Authorization', `bearer ${bearerToken}`)
                .send(event)
        ).body

        const newEvent = {
            interview: interviewId,
            initiator: '',
            eventType: 'email',
            comment: 'Changed the comment',
        }
        const validationResponse = await request(app)
            .put(`${EVENTS_ROUTE}/${eventId}`)
            .set('Authorization', `bearer ${bearerToken}`)
            .send(newEvent)

        expect(validationResponse.statusCode).to.equal(422)
        expect(validationResponse.body).to.be.an('object')
        expect(validationResponse.body.message).to.deep.equal([
            'Initiator cannot be empty',
            'Initiator has to be between 3 and 40 characters long',
        ])
    })
})
