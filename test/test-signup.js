import app from '../index.js'
import request from 'supertest'
import { expect } from 'chai'

const SIGNUP_ROUTE = '/api/signup'

describe('POST /signup', () => {
    it('Should create a new user when the correct credentials are passed', async () => {
        const user = {
            username: 'apitestsign1',
            password: 'apitestsign1',
        }
        const response = await request(app).post(SIGNUP_ROUTE).send(user)

        expect(response.statusCode).to.equal(201)
        expect(response.body).to.be.an('object')
        expect(response.body._id).to.be.a('string').and.not.be.empty
        expect(response.body.username)
            .to.be.a('string')
            .and.equal(user.username)
        expect(response.body.password)
            .to.be.a('string')
            .and.match(/^\$2a\$/, 'The string does not start with $2a$!')
    })

    it('Should return an error message when the username is already in use', async () => {
        const user = {
            username: 'apitestsign2',
            password: 'apitestsign2',
        }
        await request(app).post(SIGNUP_ROUTE).send(user)
        const response = await request(app).post(SIGNUP_ROUTE).send(user)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.eql(['Username already in use'])
    })

    it('Should an error message when empty username and empty password passed', async () => {
        const user = {
            username: '',
            password: '',
        }
        const response = await request(app).post(SIGNUP_ROUTE).send(user)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.have.members([
            'Username cannot be empty',
            'Username can contain only letters and numbers',
            'Username has to be between 3 and 20 characters long',
            'Password cannot be empty',
            'Password has to be between 3 and 20 characters long',
        ])
    })

    it('Should an error message when username contains other characters than numbers and letters', async () => {
        const user = {
            username: 'api312.*9',
            password: 'apitest',
        }
        const response = await request(app).post(SIGNUP_ROUTE).send(user)

        expect(response.statusCode).to.equal(422)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.eql([
            'Username can contain only letters and numbers',
        ])
    })
})
