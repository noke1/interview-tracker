import app from '../index.js'
import request from 'supertest'

const RESET_ROUTE = '/api/reset'

export const mochaHooks = async () => {
    const result = await request(app).get(RESET_ROUTE).expect(200)
    if (result) {
        return {
            beforeAll() {
                console.log('Database was cleared!')
            },
        }
    }
}
