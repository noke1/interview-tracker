import app from '../index.js'
import request from 'supertest'
import { expect } from 'chai'

const SIGNUP_ROUTE = '/api/signup'
const LOGIN_ROUTE = '/api/login'

describe('POST /login', () => {
    it('Should return a token when the user exists', async () => {
        const user = {
            username: 'apitestlogin1',
            password: 'apitestlogin1',
        }
        await request(app).post(SIGNUP_ROUTE).send(user)

        const response = await request(app).post(LOGIN_ROUTE).send(user)

        expect(response.statusCode).to.equal(200)
        expect(response.body).to.be.an('object')
        expect(response.body.message).to.equal('Token created!')
        expect(response.body.token).to.be.a('string').and.not.be.empty
    })

    it('Should return an error message when the user does not exist', async () => {
        const user = {
            username: 'apitestNOT_EXIST',
            password: 'apitestNOT_EXIST',
        }

        const response = await request(app).post(LOGIN_ROUTE).send(user)

        expect(response.statusCode).to.equal(400)
        expect(response.body).to.be.an('object')
        expect(response.body).to.not.have.property('token')
        expect(response.body.message).to.equal('User does not exist!')
    })

    it('Should return an error message when the password do not match', async () => {
        const user = {
            username: 'apitestlogin2',
            password: 'apitestlogin2',
        }
        await request(app).post(SIGNUP_ROUTE).send(user)

        const response = await request(app).post(LOGIN_ROUTE).send({
            username: user.username,
            password: 'WRONG_PASSWORD',
        })

        expect(response.statusCode).to.equal(400)
        expect(response.body).to.be.an('object')
        expect(response.body).to.not.have.property('token')
        expect(response.body.message).to.equal('Password does not match!')
    })
})
