# Interview tracker app

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/noke1/interview-tracker?branch=master&style=flat-square)

## App description

This is an app that somehow stores the information about the recruitment processes that you are participating.
The idea was to create an process/interview and then assign events that happen during the process (like phone call or email). So this can be treated as some sort of diary or summary od the processes.
So the app has the following endpoints available:

-   `interviews` (Available HTTP methods: GET, POST, PUT, DELETE)
-   `events` (Available HTTP methods: GET, POST, PUT, DELETE)
-   `signup` (Available HTTP methods: POST)
-   `login` (Available HTTP methods: POST)
-   `reset` (Available HTTP methods: GET) (this is mainly for testing purposes because a call to that endpoints clears all fo the collections - events, interviews, users)

## Stack

-   Node.js
-   Javascript
-   Express
-   MongoDB

## How to setup the app

### Setup the database

1. Create a free account in mongoDB service
2. Login into the account and create a first database (there is a tutorial on MongoDB that is fired up automatically)
3. Get the access to the DB:
    1. In the mongoDB site go to `DEPLOYMENT > Database`
    2. In the created database section click `Connect`
    3. Pick the option `Connect to your application > Drivers`
    4. Copy/save the connection string from the last point of the instructions. For me it looked like this:
       `mongodb+srv://nokedajunky:<password>@interviewtracker.v23fmsp.mongodb.net/?retryWrites=true&w=majority`

### Setup the app

You need to have node installed in your PC

1. Clone the repository into your local PC
2. Open the terminal and go to the main directory of the app
3. Run the command `npm install`
4. Wait for the installation to finish

### Create a .env file

The .env file is needed run run the app. It contains the variables (like username, password) that are used to setup the app.
The file that I was using looks the following:

```js
MONGODB_USERNAME=myUsername
MONGODB_PASSWORD=myPassword
MONGODB_ADDRESS=mongodb+srv://$MONGODB_USERNAME:$MONGODB_PASSWORD@interviewtracker.v23fmsp.mongodb.net/interview_tracker?retryWrites=true&w=majority
APP_SECRET=appSecret
APP_PORT=7777
```

So do the following to create a necessary .env file:

1. Go to the main folder off the app
2. Create a .env file - `touch .env`
3. Open the file and add:
    1. `MONGODB_USERNAME=myUsername` - this is a MongoDB login
    2. `MONGODB_PASSWORD=myPassword` - this is a MongoDB password
    3. `MONGODB_ADDRESS=mongodb+srv://$MONGODB_USERNAME:$MONGODB_PASSWORD@interviewtracker.v23fmsp.mongodb.net/interview_tracker?retryWrites=true&w=majority` - this is a connection string that was saved in the section `Setup the database`. Please note that the username and password in the connection string have to replaced by `$MONGODB_USERNAME` and `$MONGODB_PASSWORD` (not the values but the names as given in the example)
    4. `APP_SECRET=appSecret` - string that is used to hash the password
    5. `APP_PORT=7777` - default app port

## How to run the app

1. Open the terminal and go to the main directory off the app
2. Run the command `npm run dev`
3. Wait for the app to start - the app starts on port `7777`

## How to use the app

1. Firstly create a user account, and to achieve this send a `POST` request to `/signup` with the body:

```json
{
    "username": "test7",
    "password": "test7"
}
```

2. Obtain an authentication token by sending one more `POST` request to a `/login` with the body:

```json
{
    "username": "test7",
    "password": "test7"
}
```

In the response there is a `token` property which needs to be saved and added to each request inside the headers

3. Now if you want to use other endpoints like `interviews` or `events` attach the following `Authorization` header to the request:

```json
Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QzIiwiaWF0IjoxNjgzODAzMjQyfQ.EnxfGyNJULmo8-N5d0ZA3_qno-gn_OVIPnDTDLU7_V4
```

Where to content after 'bearer' is the token obtained in step 2

## How to run the tests

The tests are grouped into the files. There is also a possibility to run all the tests at once. I was focused on creating only the e2e tests because I am mostly interested in that topic :)

1. Open the terminal and go to the main directory off the app
2. Run the command `npm run <alias>`
    1. Possible aliases:
        1. `test-all` - runs all the tests created from all files
        2. `test-event` - runs only the tests for the `/events` endpoint
        3. `test-interview` - runs only the tests for the `/interviews` endpoint
        4. `test-signup`- runs only the tests for the `/signup` endpoint
        5. `test-login` - runs only the tests for the `/login` endpoint
3. The tests should start and the results will be visible inside the terminal
