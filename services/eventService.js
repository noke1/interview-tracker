import Event from '../models/eventModel.js'

const add = async (event) => {
    return await Event.create(event)
}

const getById = async (eventId) => {
    return await Event.findById(eventId).populate({
        path: 'interview',
        select: 'company',
    })
}

const getByIdAndDelete = async (eventId) => {
    return await Event.findByIdAndDelete(eventId)
}

const getByIdAndUpdate = async (eventId, event) => {
    return await Event.findByIdAndUpdate(eventId, event)
}

const deleteByQuery = async (query = {}) => {
    return await Event.deleteMany(query)
}

const exists = async (query = {}) => {
    return await Event.exists(query)
}

export {
    add,
    getById,
    getByIdAndDelete,
    getByIdAndUpdate,
    deleteByQuery,
    exists,
}
