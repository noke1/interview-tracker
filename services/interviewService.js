import Interview from '../models/interviewModel.js'

const add = async (interview) => {
    return await Interview.create(interview)
}

const edit = async (interview) => {
    return interview.save()
}

const getByCreator = async (creatorId) => {
    return await Interview.find({
        creator: creatorId,
    }).populate('events')
}

const getById = async (interviewId) => {
    return await Interview.findById(interviewId).populate('events')
}

const getByIdAndUpdate = async (interviewId, newInterview) => {
    return await Interview.findByIdAndUpdate(interviewId, newInterview, {
        new: true,
    }).populate('events')
}

const getByIdAndDelete = async (interviewId) => {
    return await Interview.findByIdAndDelete(interviewId)
}

const deleteByQuery = async (query = {}) => {
    return await Interview.deleteMany(query)
}

const exists = async (query = {}) => {
    return await Interview.exists(query)
}

export {
    add,
    edit,
    getByCreator,
    getById,
    getByIdAndUpdate,
    getByIdAndDelete,
    deleteByQuery,
    exists,
}
