import User from '../models/userModel.js'

const add = async (user) => {
    return await User.create(user)
}

const findOne = async (query = {}) => {
    return await User.findOne(query)
}

const deleteByQuery = async (query = {}) => {
    return await User.deleteMany(query)
}

const exists = async (query = {}) => {
    return await User.exists(query)
}

export { add, findOne, deleteByQuery, exists }
